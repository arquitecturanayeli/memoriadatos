/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "D:/practica5/arquitecturanayeli-memoriadatos-4fbc112ef631/Test2JironArchivoRam.vhd";
extern char *STD_TEXTIO;
extern char *IEEE_P_2592010699;
extern char *IEEE_P_3564397177;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
void ieee_p_3564397177_sub_1496949865_91900896(char *, char *, char *, unsigned char , unsigned char , int );
void ieee_p_3564397177_sub_2743816878_91900896(char *, char *, char *, char *);
void ieee_p_3564397177_sub_2889341154_91900896(char *, char *, char *, char *, char *);
void ieee_p_3564397177_sub_3205433008_91900896(char *, char *, char *, char *, char *, unsigned char , int );


static void work_a_2574229195_2372691052_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    int64 t7;
    int64 t8;

LAB0:    t1 = (t0 + 3784U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(54, ng0);
    t2 = (t0 + 4448);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(55, ng0);
    t2 = (t0 + 1968U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 3592);
    xsi_process_wait(t2, t8);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(56, ng0);
    t2 = (t0 + 4448);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(57, ng0);
    t2 = (t0 + 1968U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 3592);
    xsi_process_wait(t2, t8);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB8:    goto LAB2;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

}

static void work_a_2574229195_2372691052_p_1(char *t0)
{
    char t5[16];
    char t13[16];
    char t14[8];
    char t15[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t6;
    char *t7;
    int t8;
    unsigned int t9;
    unsigned char t10;
    unsigned char t11;
    char *t12;

LAB0:    t1 = (t0 + 4032U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(83, ng0);
    t2 = (t0 + 2880U);
    t3 = (t0 + 8194);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 11;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (11 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)0);
    xsi_set_current_line(84, ng0);
    t2 = (t0 + 2776U);
    t3 = (t0 + 8205);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 13;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (13 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)1);
    xsi_set_current_line(86, ng0);

LAB4:    t2 = (t0 + 2880U);
    t10 = std_textio_endfile(t2);
    t11 = (!(t10));
    if (t11 != 0)
        goto LAB5;

LAB7:    xsi_set_current_line(109, ng0);
    t2 = (t0 + 2880U);
    std_textio_file_close(t2);
    xsi_set_current_line(110, ng0);
    t2 = (t0 + 2776U);
    std_textio_file_close(t2);
    goto LAB2;

LAB5:    xsi_set_current_line(87, ng0);
    t3 = (t0 + 3840);
    t4 = (t0 + 2880U);
    t6 = (t0 + 3128U);
    std_textio_readline(STD_TEXTIO, t3, t4, t6);
    xsi_set_current_line(88, ng0);

LAB10:    t2 = (t0 + 4352);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB11;

LAB1:    return;
LAB6:;
LAB8:    t4 = (t0 + 4352);
    *((int *)t4) = 0;
    xsi_set_current_line(89, ng0);
    t2 = (t0 + 3840);
    t3 = (t0 + 3128U);
    t4 = (t0 + 2208U);
    t6 = *((char **)t4);
    t4 = (t0 + 7944U);
    ieee_p_3564397177_sub_2889341154_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(90, ng0);
    t2 = (t0 + 2208U);
    t3 = *((char **)t2);
    t2 = (t0 + 4512);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t12 = *((char **)t7);
    memcpy(t12, t3, 8U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(91, ng0);
    t2 = (t0 + 3840);
    t3 = (t0 + 3128U);
    t4 = (t0 + 2088U);
    t6 = *((char **)t4);
    t4 = (t0 + 7928U);
    ieee_p_3564397177_sub_2889341154_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(92, ng0);
    t2 = (t0 + 2088U);
    t3 = *((char **)t2);
    t2 = (t0 + 4576);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t12 = *((char **)t7);
    memcpy(t12, t3, 9U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(93, ng0);
    t2 = (t0 + 3840);
    t3 = (t0 + 3128U);
    t4 = (t0 + 2328U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_2743816878_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(94, ng0);
    t2 = (t0 + 2328U);
    t3 = *((char **)t2);
    t10 = *((unsigned char *)t3);
    t2 = (t0 + 4640);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t12 = *((char **)t7);
    *((unsigned char *)t12) = t10;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(96, ng0);

LAB14:    t2 = (t0 + 4368);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB15;
    goto LAB1;

LAB9:    t3 = (t0 + 1472U);
    t10 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t3, 0U, 0U);
    if (t10 == 1)
        goto LAB8;
    else
        goto LAB10;

LAB11:    goto LAB9;

LAB12:    t4 = (t0 + 4368);
    *((int *)t4) = 0;
    xsi_set_current_line(98, ng0);
    t2 = (t0 + 1672U);
    t3 = *((char **)t2);
    t2 = (t0 + 2448U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 8U);
    xsi_set_current_line(100, ng0);
    t2 = (t0 + 3840);
    t3 = (t0 + 3056U);
    t4 = (t0 + 2088U);
    t6 = *((char **)t4);
    memcpy(t13, t6, 9U);
    t4 = (t0 + 7928U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t13, t4, (unsigned char)0, 5);
    xsi_set_current_line(101, ng0);
    t2 = (t0 + 3840);
    t3 = (t0 + 3056U);
    t4 = (t0 + 2208U);
    t6 = *((char **)t4);
    memcpy(t14, t6, 8U);
    t4 = (t0 + 7944U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t14, t4, (unsigned char)0, 5);
    xsi_set_current_line(102, ng0);
    t2 = (t0 + 3840);
    t3 = (t0 + 3056U);
    t4 = (t0 + 2328U);
    t6 = *((char **)t4);
    t10 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_1496949865_91900896(IEEE_P_3564397177, t2, t3, t10, (unsigned char)0, 5);
    xsi_set_current_line(103, ng0);
    t2 = (t0 + 3840);
    t3 = (t0 + 3056U);
    t4 = (t0 + 2448U);
    t6 = *((char **)t4);
    memcpy(t15, t6, 8U);
    t4 = (t0 + 7960U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t15, t4, (unsigned char)0, 5);
    xsi_set_current_line(106, ng0);
    t2 = (t0 + 3840);
    t3 = (t0 + 2776U);
    t4 = (t0 + 3056U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    goto LAB4;

LAB13:    t3 = (t0 + 1472U);
    t10 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t3, 0U, 0U);
    if (t10 == 1)
        goto LAB12;
    else
        goto LAB14;

LAB15:    goto LAB13;

}


extern void work_a_2574229195_2372691052_init()
{
	static char *pe[] = {(void *)work_a_2574229195_2372691052_p_0,(void *)work_a_2574229195_2372691052_p_1};
	xsi_register_didat("work_a_2574229195_2372691052", "isim/Test2JironArchivoRam_isim_beh.exe.sim/work/a_2574229195_2372691052.didat");
	xsi_register_executes(pe);
}
