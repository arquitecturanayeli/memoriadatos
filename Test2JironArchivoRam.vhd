LIBRARY ieee;
LIBRARY STD;
USE STD.TEXTIO.ALL;
USE ieee.std_logic_TEXTIO.ALL;	--PERMITE USAR STD_LOGIC 

USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_UNSIGNED.ALL;
USE ieee.std_logic_ARITH.ALL;

ENTITY Test2JironArchivoRam IS
END Test2JironArchivoRam;
 
ARCHITECTURE behavior OF Test2JironArchivoRam IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ram
    PORT(
         dir : IN  std_logic_vector(8 downto 0);
         bus_datos_in : IN  std_logic_vector(7 downto 0);
         WD : IN  std_logic;
         clk : IN  std_logic;
         bus_datos_out : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal dir : std_logic_vector(8 downto 0) := (others => '0');
   signal bus_datos_in : std_logic_vector(7 downto 0) := (others => '0');
   signal WD : std_logic := '0';
   signal clk : std_logic := '0';

 	--Outputs
   signal bus_datos_out : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ram PORT MAP (
          dir => dir,
          bus_datos_in => bus_datos_in,
          WD => WD,
          clk => clk,
          bus_datos_out => bus_datos_out
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
	
	--variables de archivo
	file ARCH_RES :  TEXT;	--archivo de salida
	file ARCH_ENT : TEXT; --archivo de entrada	
	
	variable LINEA_RES : line;
	variable LINEA_ENT : line;
	
	-- variables de entrada
	
	variable var_dir : std_logic_vector(8 downto 0) := (others => '0');
   variable var_bus_datos_in : std_logic_vector(7 downto 0) := (others => '0');
   variable var_WD : std_logic := '0';


 	--varibales de salida
   variable var_bus_datos_out : std_logic_vector(7 downto 0);

	
   begin		
		file_open(ARCH_ENT, "ENTRADA.txt", READ_MODE); 	
		file_open(ARCH_RES, "RESULTADO.txt", WRITE_MODE); 
		
		WHILE NOT ENDFILE(ARCH_ENT) LOOP
			readline(ARCH_ENT,LINEA_ENT); 
			WAIT UNTIL RISING_EDGE(CLK);	--ESPERO AL FLANCO DE SUBIDA 
			read(LINEA_ENT, var_bus_datos_in);
			bus_datos_in <= var_bus_datos_in;
			read(LINEA_ENT, var_dir);
			dir <= var_dir;
			read(LINEA_ENT, var_WD);
			WD <= var_WD;
			
			WAIT UNTIL RISING_EDGE(CLK);	--ESPERO AL FLANCO DE SUBIDA 

			var_bus_datos_out := bus_datos_out;
			
			Hwrite(LINEA_RES, var_dir, right, 5);	--ESCRIBE EL CAMPO 
			Hwrite(LINEA_RES, var_bus_datos_in, right, 5);	--ESCRIBE EL CAMPO 
			write(LINEA_RES, var_WD, right, 5);	--ESCRIBE EL CAMPO 
			Hwrite(LINEA_RES, var_bus_datos_out, right, 5);	--ESCRIBE EL CAMPO 

			
			writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo
			
		end loop;
		file_close(ARCH_ENT);  -- cierra el archivo
		file_close(ARCH_RES);  -- cierra el archivo
			
   end process;

END;