LIBRARY ieee;
library std;
use std.textio.all;
USE ieee.std_logic_1164.ALL;
use ieee.Numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_textio.all;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY simulaciomRam IS
END simulaciomRam;
 
ARCHITECTURE behavior OF simulaciomRam IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ram
    PORT(
         bus_dir : IN  std_logic_vector(7 downto 0);
         bus_datos_in : IN  std_logic_vector(15 downto 0);
         RE : IN  std_logic;
         WE : IN  std_logic;
         clk : IN  std_logic;
         bus_datos_out : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal bus_dir : std_logic_vector(7 downto 0) := (others => '0');
   signal bus_datos_in : std_logic_vector(15 downto 0) := (others => '0');
   signal RE : std_logic := '0';
   signal WE : std_logic := '0';
   signal clk : std_logic := '0';

 	--Outputs
   signal bus_datos_out : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 50 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: RAM PORT MAP (
          bus_dir => bus_dir,
          bus_datos_in => bus_datos_in,
          RE => RE,
          WE => WE,
          clk => clk,
          bus_datos_out => bus_datos_out
        );
		  
LecEsc :process
		file ARCH_RES : TEXT;																					
		variable LINEA_RES : line;
		VARIABLE VAR_BDO : STD_LOGIC_VECTOR(15 DOWNTO 0);
			
		file ARCH_VEC : TEXT;
		variable LINEA_VEC : line;
		VARIABLE VAR_Bdir : STD_LOGIC_VECTOR(7 DOWNTO 0);
		VARIABLE VAR_BDI : STD_LOGIC_VECTOR(15 DOWNTO 0);
		VARIABLE VAR_RE : STD_LOGIC;
		VARIABLE VAR_WE : STD_LOGIC;
		VARIABLE CADENA : STRING(1 TO 5);
		
	begin
			file_open(ARCH_VEC, "./in.txt", READ_MODE);--archivo de entrada 	
			file_open(ARCH_RES, "./out.txt", WRITE_MODE);-- archivo de salida 	
		
		--Escribimos los encabezados del archivo de salida
		CADENA := " Di  ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := " A   ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := " WD  ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := " Do  ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		writeline(ARCH_RES,LINEA_RES);
		
		WAIT FOR 15 NS;
		--son 12 vectores de prueba por eso el oop de 0 a 11
		FOR I IN 0 TO 11 LOOP
		
			readline(ARCH_VEC,LINEA_VEC);--comenzamos a leer del archivo in

			hread(LINEA_VEC, VAR_BDI);
			bus_datos_in <= VAR_BDI;--asignamos al bus de datos de entrada lo que leimos del archivo in
			hread(LINEA_VEC, VAR_Bdir);
			bus_dir <= VAR_Bdir;
			read(LINEA_VEC, VAR_WE);
			WE <= VAR_WE;--vemos si hablita escritura
			
			
				WAIT UNTIL RISING_EDGE(CLK);	
			--despu�s de que procesamos los datos simplemente le asignamos la salida a el archivo out
	
			VAR_BDO := bus_datos_out;	
			
			Hwrite(LINEA_RES, VAR_BDI, 	right, 6);--comenzamos a escribir en el archivo out
			Hwrite(LINEA_RES, VAR_Bdir, 	right, 6);
			write(LINEA_RES, VAR_WE, 	right, 6);
			Hwrite(LINEA_RES, VAR_BDO, 	right, 6);
			
			writeline(ARCH_RES,LINEA_RES);
			
		end loop;
		
		file_close(ARCH_VEC);  
		file_close(ARCH_RES);  

      wait;     
	
	end process LecEsc ;





   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 


END;