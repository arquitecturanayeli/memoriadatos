
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity ram is
		GENERIC( data_n : INTEGER := 8;
					addr_n : INTEGER := 9);
    Port ( dir : in  STD_LOGIC_VECTOR (addr_n-1 downto 0);
           bus_datos_in : in  STD_LOGIC_VECTOR (data_n-1 downto 0);			  
			  WD : in  STD_LOGIC;
			  clk : in  STD_LOGIC;
           bus_datos_out : out  STD_LOGIC_VECTOR (data_n-1 downto 0));
end ram;

architecture Behavioral of ram is

type Memoria is array (0 to ((2**addr_n)-1)) of std_logic_vector (data_n-1 downto 0);
signal ram: Memoria;
begin
	process(clk)
		begin			
			if (clk'event and clk = '1') then
				if (WD = '1') then 
					ram(conv_integer(dir)) <= bus_datos_in;
				end if;
			end if;
	end process;
	bus_datos_out <= ram(conv_integer(dir));
	
end Behavioral;
