LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Test1JironRam IS
END Test1JironRam;
 
ARCHITECTURE behavior OF Test1JironRam IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ram
    PORT(
         dir : IN  std_logic_vector(8 downto 0);
         bus_datos_in : IN  std_logic_vector(7 downto 0);
         WD : IN  std_logic;
         clk : IN  std_logic;
         bus_datos_out : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal dir : std_logic_vector(8 downto 0) := (others => '0');
   signal bus_datos_in : std_logic_vector(7 downto 0) := (others => '0');
   signal WD : std_logic := '0';
   signal clk : std_logic := '0';

 	--Outputs
   signal bus_datos_out : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ram PORT MAP (
          dir => dir,
          bus_datos_in => bus_datos_in,
          WD => WD,
          clk => clk,
          bus_datos_out => bus_datos_out
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      dir <= "000000001";
      bus_datos_in <= "10001111";
		WD<= '1' ;
      wait for clk_period;	
		
		dir <= "000000011";
      bus_datos_in <= "01001111";
		WD<= '1' ;
      wait for clk_period;	
		
		dir <= "000000111";
      bus_datos_in <= "00101111";
		WD<= '1' ;
      wait for clk_period;	

      
		dir <= "000000001";
		WD<='0';
		wait for clk_period;
		
		dir <= "000000011";
		WD<='0';
		wait for clk_period;
		
		dir <= "000000111";
		WD<='0';
		wait for clk_period;

      -- insert stimulus here 

      wait;
   end process;

END;